//khai báo thư viện 

const express = require('express');


//khai báo một biến ROuter chạy trên file index.js
const photoRouter = express.Router();

photoRouter.get('/photos/',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

photoRouter.get('/photos/:photoId',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

photoRouter.post('/photos/',(request,response) => {
    response.status(200).json({
        message: 'post Method'
    })
});

photoRouter.put('/photos/:photoId',(request,response) => {
    response.status(200).json({
        message: 'put Method'
    })
});

photoRouter.delete('/photos/:photoId',(request,response) => {
    response.status(200).json({
        message: 'delete Method'
    })
});

photoRouter.get('/albums/:albumId/photos',(request,response) => {
    const albumId = request.params.albumId
    response.status(200).json({
        message: 'get Method by Iduser',
        req: albumId
    })
});

photoRouter.get('/photos-albums-query',(request,response) => {
    const albumId = request.query;
    response.status(200).json({
        message: 'get Method by Iduser',
        req: albumId
    })
});





module.exports = {
    photoRouter
}