//khai báo thư viện 

const express = require('express');


//khai báo một biến ROuter chạy trên file index.js
const userRouter = express.Router();

userRouter.get('/users/',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

userRouter.get('/users/:userId',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

userRouter.post('/users/',(request,response) => {
    response.status(200).json({
        message: 'post Method'
    })
});

userRouter.put('/users/:userId',(request,response) => {
    response.status(200).json({
        message: 'put Method'
    })
});

userRouter.get('/users/:userId',(request,response) => {
    response.status(200).json({
        message: 'delete Method'
    })
});

module.exports = {
    userRouter
}