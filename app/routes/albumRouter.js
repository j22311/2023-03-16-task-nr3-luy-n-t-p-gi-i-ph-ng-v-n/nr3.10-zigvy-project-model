//khai báo thư viện 

const express = require('express');


//khai báo một biến ROuter chạy trên file index.js
const albumRouter = express.Router();

albumRouter.get('/albums/',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

albumRouter.get('/albums/:albumId',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

albumRouter.post('/albums/',(request,response) => {
    response.status(200).json({
        message: 'post Method'
    })
});

albumRouter.put('/albums/:albumId',(request,response) => {
    response.status(200).json({
        message: 'put Method'
    })
});

albumRouter.delete('/albums/:albumId',(request,response) => {
    response.status(200).json({
        message: 'delete Method'
    })
});

albumRouter.get('/users/:userId/albums',(request,response) => {
    const userId = request.params.userId
    response.status(200).json({
        message: 'get Method by Iduser',
        req: userId
    })
});

albumRouter.get('/albums-users-query',(request,response) => {
    const userId = request.query;
    response.status(200).json({
        message: 'get Method by Iduser',
        req: userId
    })
});





module.exports = {
    albumRouter
}