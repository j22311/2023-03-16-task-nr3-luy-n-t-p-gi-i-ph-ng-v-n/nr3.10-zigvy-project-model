//khai báo thư viện 

const express = require('express');


//khai báo một biến ROuter chạy trên file index.js
const todoRouter = express.Router();

todoRouter.get('/todos/',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

todoRouter.get('/todos/:todoId',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

todoRouter.post('/todos/',(request,response) => {
    response.status(200).json({
        message: 'post Method'
    })
});

todoRouter.put('/todos/:todoId',(request,response) => {
    response.status(200).json({
        message: 'put Method'
    })
});

todoRouter.delete('/todos/:todoId',(request,response) => {
    response.status(200).json({
        message: 'delete Method'
    })
});

todoRouter.get('/users/:userId/todos',(request,response) => {
    const userId = request.params.userId
    response.status(200).json({
        message: 'get Method by Iduser',
        req: userId
    })
});

todoRouter.get('/todos-users-query',(request,response) => {
    const userId = request.query;
    response.status(200).json({
        message: 'get Method by Iduser',
        user: userId
    })
});





module.exports = {
    todoRouter
}