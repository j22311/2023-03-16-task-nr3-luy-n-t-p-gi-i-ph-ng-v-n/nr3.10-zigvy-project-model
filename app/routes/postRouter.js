//khai báo thư viện 

const express = require('express');


//khai báo một biến ROuter chạy trên file index.js
const postRouter = express.Router();

postRouter.get('/posts/',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

postRouter.get('/posts/:postId',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

postRouter.post('/posts/',(request,response) => {
    response.status(200).json({
        message: 'post Method'
    })
});

postRouter.put('/posts/:postId',(request,response) => {
    response.status(200).json({
        message: 'put Method'
    })
});

postRouter.delete('/posts/:postId',(request,response) => {
    response.status(200).json({
        message: 'delete Method'
    })
});

postRouter.get('/users/:userId/posts',(request,response) => {
    const userId = request.params.userId
    response.status(200).json({
        message: 'get Method by Iduser',
        req: userId
    })
});

postRouter.get('/posts?userId=userId',(request,response) => {
    const userId = request.query.userId;
    response.status(200).json({
        message: 'get Method by Iduser',
        req: userId
    })
});





module.exports = {
    postRouter
}