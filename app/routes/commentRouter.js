//khai báo thư viện 

const express = require('express');


//khai báo một biến ROuter chạy trên file index.js
const commentRouter = express.Router();

commentRouter.get('/comments/',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

commentRouter.get('/comments/:commentId',(request,response) => {
    response.status(200).json({
        message: 'get Method'
    })
});

commentRouter.post('/comments/',(request,response) => {
    response.status(200).json({
        message: 'post Method'
    })
});

commentRouter.put('/comments/:commentId',(request,response) => {
    response.status(200).json({
        message: 'put Method'
    })
});

commentRouter.delete('/comments/:commentId',(request,response) => {
    response.status(200).json({
        message: 'delete Method'
    })
});

commentRouter.get('/posts/:postId/comments',(request,response) => {
    response.status(200).json({
        message: 'delete Method'
    })
});

commentRouter.get('/comments?postId=:postId',(request,response) => {
    response.status(200).json({
        message: 'delete Method'
    })
});


module.exports = {
    commentRouter
}