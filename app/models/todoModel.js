const mongoose = require("mongoose");


const Schema = mongoose.Schema;

const todoSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    userId: {
        type: mongoose.Types.ObjectId,
        ref: "Users"
    },
    title: {
        type: String,
        required: true
    },
    completed: {
        type: boolean,
        required: true,
        default: false
    },
    
});

module.exports = mongoose.model('Todos', todoSchema);
