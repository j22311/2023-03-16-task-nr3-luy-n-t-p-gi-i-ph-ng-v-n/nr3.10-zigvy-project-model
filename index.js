//khai báo thư viện express
const express = require('express');

//-----b1khai báo thư viện mongoose-----
var mongoose = require('mongoose');
const { albumRouter } = require('./app/routes/albumRouter');
const { commentRouter } = require('./app/routes/commentRouter');
const { photoRouter } = require('./app/routes/photoRouter');
const { postRouter } = require('./app/routes/postRouter');
const { todoRouter } = require('./app/routes/todoRouter');
const { userRouter } = require('./app/routes/userRouter');

// khai báo app 
const app = express();

// Cấu hình request đọc được body json
app.use(express.json());

//khai báo port
const port = 8000;

app.listen(port, () => {
    console.log(`App listen on port  ${port}`)
})

// kết nối với mongoosebd
main().catch(err => console.log(err));

async function main() {
  await mongoose.connect('mongodb://127.0.0.1:27017/JSONPlaceHolder');
  console.log('Successfully connected mongoDB');
  
}
//app chạy trên userROuter
app.use('/',userRouter);

//app chạy trên postRouter
app.use('/',postRouter)

//app chạy trên commentRouter
app.use('/',commentRouter)

//app chạy trên albumRouter
app.use('/',albumRouter)

//app chạy trên photoRouter
app.use('/',photoRouter)

//app chạy trên todoRouter
app.use('/',todoRouter)